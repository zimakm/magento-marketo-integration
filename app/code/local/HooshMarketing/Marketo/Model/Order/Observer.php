<?php

class HooshMarketing_Marketo_Model_Order_Observer
{
    public function updateOpportunities($observer)
    {
        $orderId = $observer->getEvent()->getOrder()->getId();
        $model = Mage::getSingleton("hoosh_marketo/mobject");

        $model::getLeadId();

        $orders = $this->__getOrders($model);
        $model::updateOpportunity($orderId);

        if (empty($orders)) {
            $id = $orderId;
        } else {
            $id = $orders . "," . $orderId;
        }

        $model::updateLeadOrderInfo($id);
    }

    private function __getOrders($model)
    {
        $lead = $model::$config->getLead();
        $attribs = $lead->result->leadRecordList->leadRecord->leadAttributeList->attribute;
        foreach ($attribs as $attr)
        {
            if ($attr->attrName == "Magento_Sales_Orders") {
                $orders = $attr->attrValue;
                break;
            }
        }
        return $orders;
    }

}
