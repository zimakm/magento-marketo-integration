<?php
class HooshMarketing_Marketo_Model_Mapping_Classes_Customer
    extends HooshMarketing_Marketo_Model_Mapping_Classes_Abstract
{
    protected $_addressKey = "customer_address";
    protected $_key = "customer";
    /** @var  Mage_Customer_Model_Address */
    protected $_defaultAddress;
    //Takes fields to mapping from table or from attribute table
    protected $_fieldIdentifier = array(
        "country"          => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::SIMPLE_FIELD,
        "first_purchase"   => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::SIMPLE_FIELD,
        "last_purchase"    => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::SIMPLE_FIELD,
        "purchases_number" => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::SIMPLE_FIELD,
        "lifetime_sales"   => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::SIMPLE_FIELD,
        "customer"         => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::ATTRIBUTE_TYPE,
        "customer_address" => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::ATTRIBUTE_TYPE,
        "customer_entity"  => HooshMarketing_Marketo_Model_Mapping_Classes_Abstract::TABLE_TYPE
    );

    protected $_preparedCallbacks = array(
        "_country"         => "customer_address",
        "_customerAddress" => "customer",
        "_sales"           => "customer"
    );

    /**
     * @param \Varien_Object $object
     * @return \Varien_Object
     */
    protected function _cloneObject(Mage_Customer_Model_Customer $customer) {
        $this->_defaultAddress = $customer->getDefaultBillingAddress();
        return parent::_cloneObject($customer);
    }

    /**
     * @param Mage_Customer_Model_Address $address
     */
    protected function _country(&$address) {
        return array(
            "country" => Mage::app()->getLocale()->getCountryTranslation($address->getData("country_id"))
        );
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return array
     */
    protected function _sales(&$customer) {
        $orderTable = $this->_getCoreResource()->getTableName("sales_flat_order");

        $_result = $this->_getAdapter() //Aggregate functions used -> result will have only one string
            ->select()
            ->from(
                $orderTable,
                array(
                    "first_purchase"   => new Zend_Db_Expr("MIN(`created_at`)"),
                    "last_purchase"    => new Zend_Db_Expr("MAX(`created_at`)"),
                    "purchases_number" => new Zend_Db_Expr("COUNT(`created_at`)"),
                    "lifetime_sales"   => new Zend_Db_Expr("SUM(`grand_total`)")
                )
            )
            ->where("customer_id = ?", $customer->getId());

        return $this->_getAdapter()->fetchRow($_result); //setting new data to customer
    }

    /**
     * @param \Mage_Customer_Model_Customer $customer
     * @return array
     */
    protected function _customerAddress(Mage_Customer_Model_Customer &$customer) {
        $_data    = array();

        if(!$this->_defaultAddress)
            return $_data;

        //PREPARE ADDITIONAL DATA
        $this->_defaultAddress->setData($this->_addressKey, true);
        $this->_prepareData($this->_defaultAddress);

        foreach($this->_defaultAddress->getData() as $field => $value) {
            if(!$customer->hasData($field)) //replace only when customer
                $_data[$field] = $value;

        }
        
        return $_data;
    }
}